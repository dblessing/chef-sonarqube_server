# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe 'java'

sonarqube_server 'sonar' do
  baseurl 'http://sonar.example.com/downloads/rpm'
  version '5.1.2-1'
  config_options do
    # rubocop:disable Metrics/LineLength
    properties(
      'sonar' => {
        'jdbc' => {
          'username' => 'sonar_custom',
          'password' => 'super_secret',
          'url'      => 'jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance'
        },
        'web' => {
          'javaOpts' => '-Xmx1024m -XX:MaxPermSize=160m -XX:+HeapDumpOnOutOfMemoryError -server'
        }
      }
    )
    # rubocop:enable Metrics/LineLength
  end
  notifies :restart, 'service[sonar]'
end

service 'sonar' do
  action [:enable, :start]
  supports status: true, restart: true, reload: true
end
