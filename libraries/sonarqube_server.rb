# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'poise'

module SonarqubeServer
  class Resource < Chef::Resource
    include Poise

    provides :sonarqube_server
    actions :install, :remove

    attribute :name, kind_of: String
    attribute :version, kind_of: String, default: nil
    attribute :baseurl,
              kind_of: String,
              default: 'http://downloads.sourceforge.net/project/sonar-pkg/rpm'
    attribute :config,
              option_collector: true,
              template: true,
              default_source: 'sonar.properties.erb'
    attribute :user, kind_of: String, default: 'sonar'
    attribute :group, kind_of: String, default: 'sonar'

    # Take a standard Ruby hash and convert keys to a string with
    # dot-notation (java properties format)
    # For use inside default template
    def hash_to_properties_hash(hash, key = '', new_hash = {})
      hash.each do |k, v|
        # if the value is a Hash, recurse and
        # add the key to the array of parents
        if v.is_a?(Hash)
          new_key = key == '' ? k : "#{key}.#{k}"
          hash_to_properties_hash(v, new_key, new_hash)
        else
          new_hash["#{key}.#{k}"] = v
        end
      end
      new_hash
    end
  end

  class Provider < Chef::Provider
    include Poise

    provides :sonarqube_server

    def action_install
      notifying_block do
        manage_package_repo

        package 'sonar' do
          version new_resource.version
        end

        file '/opt/sonar/conf/sonar.properties' do
          owner new_resource.user
          group new_resource.group
          mode '0640'
          content new_resource.config_content
        end
      end
    end

    def action_remove
      notifying_block do
        package 'sonar' do
          action :remove
        end
      end
    end

    def manage_package_repo
      # TODO: logic for platforms
      yum_repository new_resource.name do
        description 'Sonar Yum Repository'
        baseurl new_resource.baseurl
        gpgcheck false
      end
    end
  end
end
