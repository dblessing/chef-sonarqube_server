#
# Cookbook Name:: sonarqube_server
# Spec:: sonarqube_server
#
# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'spec_helper'

describe 'sonarqube_server_test' do
  context 'with default attributes' do
    recipe 'sonarqube_server_test::default'
    step_into :sonarqube_server

    it do
      is_expected
        .to(
          create_yum_repository('sonar')
            .with_baseurl('http://downloads.sourceforge.net/project/sonar-pkg/rpm')
        )
    end

    it { is_expected.to install_package('sonar').with_version(nil) }

    it do
      is_expected
        .to(
          create_file('/opt/sonar/conf/sonar.properties')
            .with_owner('sonar')
            .with_group('sonar')
        )
    end
  end

  context 'with custom attributes' do
    recipe 'sonarqube_server_test::custom'
    step_into :sonarqube_server

    it do
      is_expected
        .to(
          create_yum_repository('sonar')
            .with_baseurl('http://sonar.example.com/downloads/rpm')
        )
    end

    it { is_expected.to install_package('sonar').with_version('5.1.2-1') }

    # rubocop:disable Metrics/LineLength
    it do
      is_expected
        .to(
          render_file('/opt/sonar/conf/sonar.properties')
            .with_content(<<EOS
# This file is managed by Chef, using the sonarqube_server cookbook.
# Editing this file by hand is highly discouraged!

sonar.jdbc.password=super_secret
sonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance
sonar.jdbc.username=sonar_custom
sonar.web.javaOpts=-Xmx1024m -XX:MaxPermSize=160m -XX:+HeapDumpOnOutOfMemoryError -server
EOS
            )
        )
    end
    # rubocop:enable Metrics/LineLength
  end
end
