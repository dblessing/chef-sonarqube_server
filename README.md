# Description

Install and configure SonarQube code analysis tool.

SonarQube requires Java but this cookbook does not install Java. If you do not
already have a Java cookbook, look at the
[Java cookbook](https://supermarket.chef.io/cookbooks/java).

Currently only supports Red Hat-based distributions. Support for Debian-based
distributions is in the plans.

# Usage

To install a stock Sonarqube server, create a recipe such as the following:

```ruby
include_recipe 'java'

# Install the latest version of SonarQube with default properties
# including H2 database (not recommended for production installation)
sonarqube_server 'sonar' do
  notifies :restart, 'service[sonar]'
end

# Create a Sonar service. This is required if you want Sonar to start.
service 'sonar' do
  action [:enable, :start]
  supports status: true, restart: true, reload: true
end
```

The following snippet shows how to customize the installation including
configuration properties. The `properties` attribute under `config_options`
should be created as a Ruby hash where the nested hashes represent the dot-notated
property key. For example, the property key for database username is
`sonar.jdbc.username` and is represented in the Ruby hash as seen below. Any
configuration property Sonar supports will work.

```ruby
sonarqube_server 'sonar' do
  baseurl 'http://sonar.example.com/downloads/rpm'
  version '5.1.2-1'
  config_options do
    properties(
      'sonar' => {
        'jdbc' => {
          'username' => 'sonar_custom',
          'password' => 'super_secret',
          'url'      => 'jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance'
        },
        'web' => {
          'javaOpts' => '-Xmx1024m -XX:MaxPermSize=160m -XX:+HeapDumpOnOutOfMemoryError -server'
        }
      }
    )
  end
  notifies :restart, 'service[sonar]'
end
```

# Development

## Generating Documentation
DO NOT EDIT THIS README.md file directly. This file is generated using knife-cookbook-doc plugin.
Install this plugin with `gem install knife-cookbook-doc`.
Documentation is compiled from the following sources:

1. Derived for attributes/recipes either by scanning the source code or by explicit declaration
in metadata.rb
1. Markdown files in the doc/ directory (overview is always the first to be compiled)

To edit this README:

1. Change relevant sections within the markdown files in the doc/ directory
1. Edit metadata.rb or use inline annotated comments within the source code.
For more info on inline comments
[click here](https://confluence.wsd.bucklehq.com/display/SysAdmin/Auto-generated+Documentation#Auto-generatedDocumentation-Inlinecomments)
1. Generate new README using knife-cookbook-doc plugin and push changes to remote branch.
For more info on using the knife-cookbook-doc plugin
[click here.](https://confluence.wsd.bucklehq.com/display/SysAdmin/Auto-generated+Documentation#Auto-generatedDocumentation-Usage)

# Testing

## Code Style
To run style tests (Rubocop and Foodcritic):
`rake style`

If you want to run either Rubocop or Foodcritic separately, specify the style
test type (Rubocop = ruby, Foodcritic = chef)
`rake style:chef`
or
`rake style:ruby`

## RSpec tests
Run RSpec unit tests
`rake spec`

## Test Kitchen
Run Test Kitchen tests (these tests take quite a bit of time)
`rake integration:vagrant`

If the cookbook has tests that run in EC2
`rake integration:cloud`

# Forking

If you choose to fork this cookbook here are some good tips to keep things in
order

1. Fork the cookbook *before* cloning.
1. Clone the *forked* repo, not the original.
1. Once the fork is cloned, go to the repo directory and add an `upstream`
remote
`git remote add upstream git@gitlab.example.com:cookbooks/this_cookbook.git`

Now you can pull `upstream` changes (things merged into the main cookbook repo).
Note that you will also need to push to your fork's master to keep it updated.
The alias below will help you. After adding the alias you will simply be able to
run `git-reup` and it will pull the upstream changes and push them to
your fork. Then checkout a branch and work as normal.

Add the following alias in `~/.bash_profile`.
`alias git-reup='git checkout master && git pull upstream master && git push origin master'`

# Requirements

## Platform:

*No platforms defined*

## Cookbooks:

* poise (~> 2.0)
* java (~> 1.31)
* yum (~> 3.6)

# Attributes

*No attributes defined*

# Recipes

*No recipes defined*

# License and Maintainer

Maintainer:: Drew A. Blessing (<cookbooks@blessing.io>)

License:: Apache 2
