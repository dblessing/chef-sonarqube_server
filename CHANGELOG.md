# Changelog

0.1.1
---------
- Fix incorrect maintainer name and license in metadata and README.

0.1.0
---------
- Initial release
